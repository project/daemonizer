<?php

namespace Drupal\daemonizer;

use Drupal\Core\Lock\LockBackendInterface;
use React\EventLoop\Loop;
use Symfony\Component\Console\Helper\ProgressIndicator;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Perform a task periodically whilst offering cooperative handover.
 */
class DaemonizedTask {

  /**
   * Task to perform periodically.
   *
   * @var callable
   */
  protected $task;

  /**
   * Intervals and lock identifiers for the periodic task execution.
   *
   * @var \Drush\Commands\daemonizer\DaemonizerConfig
   */
  protected $config;

  /**
   * The Symfony console output service, used to manage the progress indicator.
   *
   * @var \Symfony\Component\Console\Output\OutputInterface
   */
  private $output;

  /**
   * Symfony console progress indicator to show the command is performing.
   *
   * @var \Symfony\Component\Console\Helper\ProgressIndicator
   */
  private $progressIndicator;

  /**
   * The Lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  private $lockService;

  /**
   * The event loop.
   *
   * @var \React\EventLoop\Loop
   */
  private $loop;

  /**
   * Timers in each event loop.
   *
   * @var \React\EventLoop\TimerInterface[]
   */
  private $timers = [];

  /**
   * When the service is ready to handover, release the lock between runs.
   *
   * @var bool
   */
  private $readyToHandover = FALSE;

  /**
   * Constructor.
   *
   * @param callable $task
   *   Task to perform periodically.
   * @param \Drupal\daemonizer\DaemonizerConfig $config
   *   The daemonization configuration for the background task.
   */
  public function __construct($task, DaemonizerConfig $config) {
    $this->task = $task;
    $this->config = $config;
  }

  /**
   * Set the Lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface $lock
   *   The Lock service.
   */
  public function setLockService(LockBackendInterface $lock) {
    $this->lockService = $lock;
  }

  /**
   * Set the console output service.
   *
   * @var \Symfony\Component\Console\Output\OutputInterface $output
   *   The console output service.
   */
  public function setOutput(OutputInterface $output) {
    $this->output = $output;
  }

  /**
   * Start running the task.
   */
  public function run() {
    // The init method will check that the lock is available, or decide to wait
    // during the handover phase.
    if (!$this->init()) {
      return FALSE;
    }

    // The shutdown function should be registered *after* the init function,
    // so that if the init fails, the locks will not be arbitrarily removed.
    drupal_register_shutdown_function([$this, 'cleanupOnShutdown']);

    $this->loop = Loop::get();
    $this->timers = [
      'progress' => $this->loop->addPeriodicTimer(0.1, [
        $this,
        'advanceProgress',
      ]),
      'task' => $this->loop->addPeriodicTimer($this->config->taskInterval, [
        $this,
        'performTask',
      ]),
      'heartbeat' => $this->loop->addPeriodicTimer($this->config->heartbeatInterval, [
        $this,
        'beat',
      ]),
      'signalHandover' => $this->loop->addTimer($this->config->handoverPointInSeconds, [
        $this,
        'signalHandover',
      ]),
      'maxExecutionTime' => $this->loop->addTimer($this->config->maxExecutionTime, [
        $this,
        'stop',
      ]),
    ];

    $this->loop->run();
    return TRUE;
  }

  /**
   * Provide reassurance for infrequently-performed tasks.
   */
  public function advanceProgress() {
    $this->progressIndicator->advance();
  }

  /**
   * Perform the periodic task.
   */
  public function performTask() {
    $this->progressIndicator->advance();

    if ($this->readyToHandover) {
      if (!$this->acquireHeartbeatLock()) {
        $this->stop();
        return;
      }
    }

    $task = $this->task;
    $task();

    // During the handover state, release the lock between runs.
    if ($this->readyToHandover) {
      $this->releaseHeartbeatLock();
    }
  }

  /**
   * Acquire (or re-acquire) a heartbeat lock.
   *
   * If the lock can't be acquired the service is shutdown.
   */
  public function beat() {
    if (!$this->acquireHeartbeatLock()) {
      $this->stop();
    }
    $this->progressIndicator->advance();
  }

  /**
   * Signal to other instances that the service is ready to handover.
   */
  public function signalHandover() {
    $this->readyToHandover = TRUE;
    $this
      ->loop
      ->cancelTimer($this->timers['heartbeat']);
    $this->acquireHandoverSignalLock();
  }

  /**
   * Shut down the service.
   */
  public function stop() {
    foreach ($this->timers as $timer) {
      $this->loop->cancelTimer($timer);
    }
  }

  /**
   * Close locks on shutdown.
   */
  public function cleanupOnShutdown() {
    $this->releaseHeartbeatLock();
    $this->releaseHandoverSignalLock();
  }

  /**
   * Attempt to acquire the heartbeat lock.
   *
   * @return bool
   *   TRUE if the lock was successfully acquired.
   */
  private function acquireHeartbeatLock() : bool {
    // Take the lock for twice the time between each heartbeat, to ensure
    // renewal without a gap.
    return $this
      ->lockService()
      ->acquire($this->config->lockNameHeartbeat, (2 * $this->config->heartbeatInterval));
  }

  /**
   * Attempt to acquire the heartbeat lock and be prepared to wait.
   *
   * @return bool
   *   TRUE if the lock was successfully acquired.
   */
  private function waitForHeartbeatLock() : bool {
    if ($this->acquireHeartbeatLock()) {
      return TRUE;
    }

    $this
      ->lockService()
      ->wait($this->config->lockNameHeartbeat, (2 * $this->config->heartbeatInterval));
    return $this->acquireHeartbeatLock();
  }

  /**
   * Attempt to acquire the lock which signals readiness to handover.
   */
  private function acquireHandoverSignalLock() : void {
    // Take the lock for the remaining time in maximum execution.
    $duration = ($this->config->maxExecutionTime * (1 - $this->config->handoverPoint));
    $this
      ->lockService()
      ->acquire($this->config->lockNameHandoverSignal, $duration);
  }

  /**
   * Validate whether the handover signal lock is present.
   *
   * @return bool
   *   TRUE if the service is ready to handover.
   */
  private function isShowingHandoverSignal() : bool {
    $result = !$this
      ->lockService()
      ->lockMayBeAvailable($this->config->lockNameHandoverSignal);
    return $result;
  }

  /**
   * Release the heartbeat lock.
   */
  private function releaseHeartbeatLock() {
    $this
      ->lockService()
      ->release($this->config->lockNameHeartbeat);
  }

  /**
   * Release the handover signal lock.
   */
  private function releaseHandoverSignalLock() {
    $this
      ->lockService()
      ->release($this->config->lockNameHandoverSignal);
  }

  /**
   * Get the lock service.
   *
   * @return \Drupal\Core\Lock\LockBackendInterface
   *   The lock service.
   */
  private function lockService() : LockBackendInterface {
    if (empty($this->lockService)) {
      $this->lockService = \Drupal::lock();
    }
    return $this->lockService;
  }

  /**
   * Initialize the lock names, if not provided.
   */
  private function init() {
    $taskName = $this->getCallableName($this->task);
    if (empty($this->config->lockNameHandoverSignal)) {
      $this->config->lockNameHandoverSignal = $taskName . '-ready-for-handover';
    }
    if (empty($this->config->lockNameHeartbeat)) {
      $this->config->lockNameHeartbeat = $taskName . '-heartbeat';
    }

    $this->progressIndicator = new ProgressIndicator(
      $this->output,
      'verbose',
      100,
      ['⠏', '⠛', '⠹', '⢸', '⣰', '⣤', '⣆', '⡇']
    );

    if ($this->acquireHeartbeatLock()) {
      $this->progressIndicator->start('Processing...');
      return TRUE;
    }

    // If the running service is offering handover, wait to take over.
    if ($this->isShowingHandoverSignal()) {
      $this->progressIndicator->start('Processing…');
      return $this->waitForHeartbeatLock();
    }
    return FALSE;
  }

  /**
   * Auto-generate lock-names using the identity of the passed callable.
   *
   * @var callable $callable
   *   The task to perform periodically.
   *
   * @return string
   *   The name to use for the lock.
   */
  private function getCallableName($callable) {
    if (!is_callable($callable)) {
      throw new \Exception('The task is not callable.');
    }
    switch (TRUE) {
      case is_string($callable) && strpos($callable, '::'):
        return '[static] ' . $callable;

      case is_string($callable):
        return '[function] ' . $callable;

      case is_array($callable) && is_object($callable[0]):
        return '[method] ' . get_class($callable[0]) . '->' . $callable[1];

      case is_array($callable):
        return '[static] ' . $callable[0] . '::' . $callable[1];

      case $callable instanceof \Closure:
        return '[closure]';

      case is_object($callable):
        return '[invokable] ' . get_class($callable);

      default:
        return '[unknown]';
    }
  }

}
