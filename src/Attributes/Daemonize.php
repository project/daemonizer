<?php

namespace Drupal\daemonizer\Attributes;

use Consolidation\AnnotatedCommand\Parser\CommandInfo;

/**
 * Mark a Drush command for periodic background processing.
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
class Daemonize {

  /**
   * {@inheritdoc}
   */
  public static function handle(\ReflectionAttribute $attribute, CommandInfo $commandInfo) {
    $commandInfo->addAnnotation('daemonize', TRUE);
    $commandInfo->addAnnotation('daemonize:options', $attribute->getArguments());
  }

}
