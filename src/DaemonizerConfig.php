<?php

namespace Drupal\daemonizer;

/**
 * Configuration provider for the Daemonizer service.
 */
class DaemonizerConfig {

  /**
   * Prefix to use when auto-generating lock names.
   */
  const LOCK_NAME_PREFIX = 'daemonize';

  /**
   * Suffix to use with an auto-generated heartbeat lock.
   */
  const LOCK_NAME_SUFFIX_HEARTBEAT = 'heartbeat';

  /**
   * Suffix to use with an auto-generated handover lock.
   */
  const LOCK_NAME_SUFFIX_HANDOVER = 'handover';

  /**
   * Provide the options available for a Daemonized task.
   *
   * An array providing the name, description, and (optionally) the default
   * value for each command-line option.
   *
   * @const array
   */
  const DAEMONIZATION_OPTIONS = [
    'lockNameBase' => [
      'description' => 'The base lock name for auto-generating locks',
      'defaultValue' => '',
    ],
    'lockNameHeartbeat' => [
      'description' => 'Lock name for the heartbeat lock',
      'defaultValue' => '',
    ],
    'lockNameHandoverSignal' => [
      'description' => 'Lock name for the handover signal',
      'defaultValue' => '',
    ],
    'taskInterval' => [
      'description' => 'Frequency (in seconds) to perform the task',
      'defaultValue' => 5,
    ],
    'heartbeatInterval' => [
      'description' => 'Frequency (in seconds) to report the keep-alive signal',
      'defaultValue' => 5,
    ],
    'maxExecutionTime' => [
      'description' => 'Maximum time (in seconds) to run',
      'defaultValue' => 60,
    ],
    'handoverPoint' => [
      'description' => 'Point at which the daemon should signal readiness to handover',
      'defaultValue' => 0.7,
    ],
  ];

  /**
   * Base name of the locks, used for auto-generating lock names.
   *
   * @var string
   */
  protected string $lockNameBase = self::DAEMONIZATION_OPTIONS['lockNameBase']['defaultValue'];

  /**
   * Name of the heartbeat lock.
   *
   * @var string
   */
  protected string $lockNameHeartbeat = self::DAEMONIZATION_OPTIONS['lockNameHeartbeat']['defaultValue'];

  /**
   * Name of the handover signal lock.
   *
   * @var string
   */
  protected string $lockNameHandoverSignal = self::DAEMONIZATION_OPTIONS['lockNameHandoverSignal']['defaultValue'];

  /**
   * Perform the task every 3 seconds.
   *
   * @var int
   */
  protected int $taskInterval = self::DAEMONIZATION_OPTIONS['taskInterval']['defaultValue'];

  /**
   * Report keep-alive every 10 seconds.
   *
   * @var int
   */
  protected int $heartbeatInterval = self::DAEMONIZATION_OPTIONS['heartbeatInterval']['defaultValue'];

  /**
   * Run for 60 seconds.
   *
   * @var int
   */
  protected int $maxExecutionTime = self::DAEMONIZATION_OPTIONS['maxExecutionTime']['defaultValue'];

  /**
   * Signal ready to handover at 50% of the maximum execution time.
   *
   * @var float
   */
  protected float $handoverPoint = self::DAEMONIZATION_OPTIONS['handoverPoint']['defaultValue'];

  /**
   * {@inheritdoc}
   */
  public function set($key, $value) {
    if (strpos($key, 'daemonize:') === 0) {
      $key = substr($key, 10);
    }

    if (property_exists($this, $key)) {
      switch ($key) {
        case 'lockNameBase':
        case 'lockNameHeartbeat':
        case 'lockNameHandoverSignal':
          if (!is_string($value)) {
            throw new \Exception('The lock identifier must be a string.');
          }
          break;

        case 'taskInterval':
        case 'heartbeatInterval':
        case 'maxExecutionTime':
          $value = intval($value);
          if ($value < 1) {
            throw new \Exception('The period must be an integer giving the time in seconds.');
          }
          break;

        case 'handoverPoint':
          $value = (float) $value;
          if ($value < 0 || $value > 1) {
            throw new \Exception('The handover point must be a percentage, indicated as a number between 0 and 1.');
          }
          break;
      }

      $this->$key = $value;
      return $this;
    }

    throw new \Exception(sprintf('The property "%s" is not applicable.', $key));
  }

  /**
   * {@inheritdoc}
   */
  public function get($key) {
    switch ($key) {
      case 'handoverPointInSeconds':
        return $this->maxExecutionTime * $this->handoverPoint;

      // Use the base lock to auto-generate the heartbeat and handover locks.
      case 'lockNameHeartbeat':
        if (!empty($this->$key)) {
          return $this->$key;
        }
        return $this->generateLockName(self::LOCK_NAME_SUFFIX_HEARTBEAT);

      case 'lockNameHandoverSignal':
        if (!empty($this->$key)) {
          return $this->$key;
        }
        return $this->generateLockName(self::LOCK_NAME_SUFFIX_HANDOVER);
    }

    if (property_exists($this, $key) && $this->$key !== NULL) {
      return $this->$key;
    }

    throw new \Exception(sprintf('The property "%s" is not applicable.', $key));
  }

  /**
   * {@inheritdoc}
   */
  public function __set($key, $value) {
    $this->set($key, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function __get($key) {
    return $this->get($key);
  }

  /**
   * {@inheritdoc}
   */
  public function __isset($key) {
    switch ($key) {
      case 'handoverPointInSeconds':
        return TRUE;

      // The base lock name auto-generates the heartbeat and handover locks.
      case 'lockNameHeartbeat':
      case 'lockNameHandoverSignal':
        return !empty($this->lockNameBase);
    }

    if (property_exists($this, $key) && !is_null($this->$key)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function generateLockName($suffix) : string {
    if (empty($this->lockNameBase)) {
      throw new \Exception(sprintf('A lock name is required.'));
    }

    return sprintf('%s:%s:%s', self::LOCK_NAME_PREFIX, $this->lockNameBase, $suffix);
  }

}
