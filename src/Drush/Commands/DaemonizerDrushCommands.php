<?php

namespace Drupal\daemonizer\Drush\Commands;

use Consolidation\AnnotatedCommand\AnnotatedCommand as Command;
use Consolidation\AnnotatedCommand\AnnotationData;
use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\daemonizer\DaemonizedTask;
use Drupal\daemonizer\DaemonizerConfig;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Psr\Container\ContainerInterface as DrushContainer;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Intercept Drush commands to choose whether to operate them as a daemon.
 */
class DaemonizerDrushCommands extends DrushCommands {

  /**
   * Drupal lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The Drupal lock service.
   */
  public function __construct(LockBackendInterface $lock) {
    $this->lock = $lock;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, DrushContainer $drush): self {
    return new static($container->get('lock'));
  }

  /**
   * Add options to the commands tagged with the `Daemonize` attribute.
   */
  #[CLI\Hook(type: HookManager::OPTION_HOOK, selector: 'daemonize')]
  public function addOptions(Command $command, AnnotationData $annotationData) {
    // Add '--daemonize' as an option so that the task can be de-daemonized.
    // The '--no-daemonize' option is automatically provisioned by Drush.
    $command->addOption(
      'daemonize',
      NULL,
      InputOption::VALUE_NONE | InputOption::VALUE_NEGATABLE,
      'Execute this command as a recurring task.'
    );

    // Allow the config to be managed directly in the attribute definition.
    $attributeDefaults = $annotationData->getList('daemonize:options');

    // Provide a default lock base-name from the name of the drush command.
    if (!array_key_exists('lockNameBase', $attributeDefaults)) {
      $attributeDefaults['lockNameBase'] = $command->getName();
    }

    // Add all the daemonization parameters as command-line options.
    foreach (DaemonizerConfig::DAEMONIZATION_OPTIONS as $key => $option) {
      $name = 'daemonize:' . $key;

      // Override the default value, if it's given in the Attribute definition.
      if (array_key_exists($key, $attributeDefaults)) {
        $option['defaultValue'] = $attributeDefaults[$key];
      }
      $command->addOption(
        $name,
        NULL,
        InputOption::VALUE_REQUIRED,
        $option['description'],
        $option['defaultValue'] ?? NULL
      );
    }
  }

  /**
   * Attach the callback closure as late as possible.
   */
  #[CLI\Hook(type: HookManager::POST_OPTION_HOOK, selector: 'daemonize')]
  public function attachCallableTask(Command $command) {
    $reflection = new \ReflectionClass($command);
    $property = $reflection->getProperty('commandCallback');
    $callable = $property->getValue($command);

    $command
      ->getAnnotationData()
      ->set('daemonize:callable', $callable);
  }

  /**
   * Validate that a lock-name definition has been provided.
   */
  #[CLI\Hook(type: HookManager::ARGUMENT_VALIDATOR, selector: 'daemonize')]
  public function validateLockNames(CommandData $commandData) {
    $options = $commandData
      ->input()
      ->getOptions();

    if (!empty($options['daemonize:lockNameHeartbeat']) && !empty($options['daemonize:lockNameHandoverSignal'])) {
      return TRUE;
    }

    if (empty($options['daemonize:lockNameBase'])) {
      throw new \Exception('Required options: either a base lock name, or a heartbeat lock name and also a handover signal lock name.');
    }
  }

  /**
   * Intercept commands which are tagged with the `Daemonize` attribute.
   */
  #[CLI\Hook(type: HookManager::PRE_COMMAND_HOOK, selector: 'daemonize')]
  public function daemonize(CommandData $commandData) {
    $options = $commandData
      ->input()
      ->getOptions();

    if (!array_key_exists('daemonize', $options) || $options['daemonize'] === FALSE) {
      return;
    }

    $options = array_filter($options, fn($key) => (strpos($key, 'daemonize:') === 0), ARRAY_FILTER_USE_KEY);
    $options = array_filter($options, fn($value) => ($value !== FALSE));

    $config = new DaemonizerConfig();
    foreach ($options as $key => $value) {
      $config->set($key, $value);
    }

    $callable = $commandData->annotationData()['daemonize:callable'];

    $task = new DaemonizedTask($callable, $config);
    $task->setLockService($this->lock);
    $task->setOutput($commandData->output());

    if (!$task->run()) {
      $this->io()->warning('The task is already running.');
    }

    // Prevent the daemonized task from running as a standalone task.
    return FALSE;
  }

}
